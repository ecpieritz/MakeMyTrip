<h1 align = "center"> :fast_forward: Make My Trip :rewind: </h1>

## 🖥 Preview
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/125488203_1789382671216076_2486146820149536266_o.jpg?_nc_cat=103&ccb=2&_nc_sid=0debeb&_nc_eui2=AeE672k7TppA-Zej_PiW_UUmAiEEqIi6BtsCIQSoiLoG26SRqUihAxRk0pQrqQV3nIShz_Qw_KsbJTua_tbB_zyQ&_nc_ohc=Y7jBx_HAUgQAX91lQAU&_nc_ht=scontent.fbnu2-1.fna&oh=8bf773173e04ff4ded08cf33db565ba8&oe=5FD7416E" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/125776987_1789382651216078_4796859086493631707_o.jpg?_nc_cat=106&ccb=2&_nc_sid=0debeb&_nc_eui2=AeGBjsIASCNTOX8MS7BpSxd_TD2NgKl1iuVMPY2AqXWK5cGsCIKp0IMBPo5Bwp4EXWX-3vEsZ1eRDQPWWRx7zP31&_nc_ohc=Rao_NLndLlIAX-sJf8x&_nc_ht=scontent.fbnu2-1.fna&oh=39d5e6946bfd8c0953e0cab946e20472&oe=5FD770B1" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/125464500_1789382631216080_5405307119405661906_o.jpg?_nc_cat=101&ccb=2&_nc_sid=0debeb&_nc_eui2=AeHStSunmRcY-GBElIr7fWOz8Flqug0SWBDwWWq6DRJYEGaJtG9xUnnb3rAsNct0gVCg6wGiN1q8uwY3tRi8MNKG&_nc_ohc=rDOcEdrdnVEAX_JO5Ke&_nc_ht=scontent.fbnu2-1.fna&oh=01214f8faa3b02a32927889f6c20366f&oe=5FD7F2BF" width = "500">
</p>
---

## 📖 About
<p>Travel agency for exclusive trips to some Asian countries. Site created to train knowledge in Bootstrap 4 and only developed the frontend.</p>

---

## 🛠 Technologies used
- CSS
- HTML
- Bootstrap v4.5.3

---


## 🚀 How to execute the project
#### Clone the repository
git clone https://github.com/EPieritz/MakeMyTrip.git

#### Enter directory
`cd MakeMyTrip`

#### Run the server
- right click on the `index.html` file
- click on `open with liveserver`

---
Developed with 💙 by Emilyn C. Pieritz
